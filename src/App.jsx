// React core
import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Components
import HomePage from "./components/template/HomePage";
import VideoPage from "./components/template/VideoPage";
import SearchPage from "./components/template/SearchPage";

// Other
import information from "./information.json";
import "./css/style.css";

export default function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route
            path="/"
            exact
            render={() => <HomePage information={information} />}
            information={information}
          />
          <Route
            path="/video/:id"
            render={({ match }) => (
              <VideoPage match={match} information={information} />
            )}
            information={information}
          />
          <Route
            path="/search"
            render={() => <SearchPage information={information} />}
          />
        </Switch>
      </div>
    </Router>
  );
}
