// React core
import React from "react";

// Components
import Header from "../organisms/Header";

export default function SearchPage() {
  return (
    <div className="search-page">
      <Header />
      <h1>Search results will go here</h1>
    </div>
  );
}
