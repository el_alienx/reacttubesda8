# ReactTube SDA8

This project is the updated version of the ReactTube used in the past year. This includes all the current practices to avoid having any problem with current libraries like:

- Node
- Webpack
- React
- React Router DOM

## Orginial website (for posterity)

This is the original link, even if the codebase is old, the project is complete and by used to see the expected functionality: [ReactTube](https://react-tube-19e9e.web.app)

## Instructions

1. To install this project write `npm install` in the console to get all the Node dependecies beforehand.
1. To run the project write `npm start` in the console to start the project.

## Resources

In case you want to start your project from start the following link contains a zip file with the following files:

- [resources.zip](https://firebasestorage.googleapis.com/v0/b/react-tube-19e9e.appspot.com/o/resources.zip?alt=media&token=9659e6db-3642-4330-9767-110ced597c68): Contains the files that will not be loaded from a server.
- [Atomic design reference.png](https://firebasestorage.googleapis.com/v0/b/react-tube-19e9e.appspot.com/o/atomic-design-react.png?alt=media&token=b6d773f1-aa11-4873-b4af-efe159aceffe): A visual guide to visualize the relation between Atomic design and React Components.
- [Project management.numbers](https://www.icloud.com/numbers/0vtXIXP9mEIT_ICJB5jxjsFPg#ReactTube_SDA8_Project): A iCloud link to view the project management file online.
